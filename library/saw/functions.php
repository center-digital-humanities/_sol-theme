<?php

function print_array($array){
    echo "<pre>";
    var_dump($array);
    echo "</pre>";
}

function get_academic_year_range($fromCurrent = 0){

    $current_year = date('Y')+$fromCurrent;
    $current_month = date('n');

    if($current_month >=8){
        $year_range = $current_year . '-'. ($current_year+1);
    }
    else{
        $year_range = ($current_year-1) . '-'. $current_year;
    }

    return $year_range;
}

function get_first_term_of_academic_year($fromCurrent = 0){
    $current_year = date('Y')+$fromCurrent;
    $current_month = date('n');

    if($current_month >=8){
        $year = $current_year;
    }
    else{
        $year = ($current_year-1);
    }

    return 'FALL '.$year;
}

function first_term_of_academic_year($fromCurrent = 0){
    $current_year = date('Y')+$fromCurrent;
    $current_month = date('n');

    if($current_month >=8){
        $year = $current_year;
    }
    else{
        $year = ($current_year-1);
    }

    return 'FALL '.$year;
}

function make_post_friendly($string){
    return str_replace(' ', '%20',$string);
}

function newline(){
    //if run in cli
    if(php_sapi_name()==="cli") {
        $newline = "\n";
    } else {
        $newline = "<br>";
    }
    return $newline;
}