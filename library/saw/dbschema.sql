CREATE TABLE subject_area (
    id int(11) NOT NULL AUTO_INCREMENT,
    code varchar(20) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (code)
);

CREATE TABLE term (
    id int(11) NOT NULL AUTO_INCREMENT,
    code varchar(5) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (code)
);

CREATE TABLE course (
    id int(11) NOT NULL AUTO_INCREMENT,
    subject_area_id int(11) NOT NULL,
    career_level varchar(5) NOT NULL,
    catalog_number varchar(11) NOT NULL,
    catalog_number_display varchar(11) NOT NULL,
    short_title varchar(255) NOT NULL,
    long_title varchar(255) NOT NULL,
    description varchar(1010) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (subject_area_id) REFERENCES subject_area(id),
    UNIQUE (subject_area_id, catalog_number)
);

CREATE TABLE class (
    id int(11) NOT NULL AUTO_INCREMENT,
    course_id int(11) NOT NULL,
    term_id int(11) NOT NULL,
    class_number varchar(11) NOT NULL,
    title varchar(255) NOT NULL,
    description varchar(1010) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (course_id) REFERENCES course(id) ON DELETE CASCADE,
    FOREIGN KEY (term_id) REFERENCES term(id),
    UNIQUE (course_id, term_id, class_number)
);

CREATE TABLE class_section (
    id int(11) NOT NULL AUTO_INCREMENT,
    class_id int(11) NOT NULL,
    section_number varchar(11) NOT NULL,
    section_id int(11) NOT NULL,
    enrollment_status varchar(2) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (class_id) REFERENCES class(id) ON DELETE CASCADE
);

CREATE TABLE instructor (
    uclaid varchar(11) NOT NULL,
    first_name varchar(25) NOT NULL,
    middle_name varchar(25),
    last_name varchar(25) NOT NULL,
    full_name varchar(80) NOT NULL,
    PRIMARY KEY (uclaid)
);

CREATE TABLE section_instructor (
    class_section_id int(11) NOT NULL,
    uclaid varchar(11) NOT NULL,
    FOREIGN KEY (class_section_id) REFERENCES class_section(id) ON DELETE CASCADE
);
